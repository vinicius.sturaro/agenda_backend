const Agenda = require('./crud')
Agenda.methods(['get','post','put','delete'])
Agenda.updateOptions({new: true, runValidations: true})
module.exports = Agenda


