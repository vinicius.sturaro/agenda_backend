const restful = require('node-restful')
const mongoose = restful.mongoose
const crudSchema = new mongoose.Schema({
    name: {type: String},
    email: {type: String},
    avatarURL: {type: String}
})

module.exports = restful.model('Agenda', crudSchema)
