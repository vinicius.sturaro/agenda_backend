const express = require('express')
module.exports = function(server) {
    
    const router = express.Router()
    server.use('/api', router)

    const crudService = require('../api/agenda/crudService')
    crudService.register(router, '/agenda')

}